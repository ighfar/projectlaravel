<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->integer('id');
            $table->string('judul',45);
            $table->text('ringkasan');
            $table->integer('harga');
            $table->string('poster',45);
            $table->unsignedInteger('genre_id')->nullable();
            $table->foreign('genre_id')
                ->on('genres')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
             $table->timestamps();
        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
