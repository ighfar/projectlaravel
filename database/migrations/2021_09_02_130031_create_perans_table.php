<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perans', function (Blueprint $table) {
            $table->integer('id');
           $table->unsignedInteger('film_id')->nullable();
            $table->foreign('film_id')
                ->on('films')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
          $table->unsignedInteger('cast_id')->nullable();
          $table->foreign('cast_id')
                ->on('casts')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('nama',45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perans');
    }
}
